import moment from 'moment';

export const getDaysBetweenDates = (startDate, endDate) => {

  const now   = startDate.clone(),
        dates = [];

  while (now.isSameOrBefore(endDate)) {
    dates.push(now.clone());
    now.add(1, 'days');
  }

  return dates;

};

export const weekHeaders = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];
export const getWeeksInRange = (dateRange) => {

  const weeks = dateRange.reduce((weeks, day) => {

    const isTurnoverWeek = day.weekYear() !== day.year();

    const week = day.week(),
          year = isTurnoverWeek ? day.weekYear() : day.year();

    const weekLabel = `${week}-${year}`;

    return [
      ...weeks,
      ...(!weeks.includes(weekLabel) ? [ weekLabel ] : [ ])
    ];

  }, [ ]);

  return weeks;

};
export const getWeekRange = (weekLabel) => {
  
  const [ week, year ] = weekLabel.split('-');

  const weekStart = moment(year, 'YYYY').week(week).day(0),
        weekEnd   = moment(year, 'YYYY').week(week).day(6);

  return getDaysBetweenDates(weekStart, weekEnd);

};