import React, { useState } from 'react';

import Calendar from './Calendar';

import { weekHeaders } from 'utils/dates';

import style from './index.module.scss';

const App = () => {

  const [ monthHeader, setMonthHeader ] = useState('');

  return (
    <div className={style.wrap}>
      <div className={style.wrapMonthHeader}>
        <h2>{monthHeader}</h2>
      </div>

      <div className={style.wrapWeekHeaders}>
        {weekHeaders.map((label) => (
          <div
            key={`week-header-${label}`}
            className={style.weekHeader}>
            {label}
          </div>
        ))}
      </div>

      <Calendar onUpdateMonthHeader={setMonthHeader} />
    </div>
  );
}

export default App;
