import React from 'react';
import moment from 'moment';

import style from './index.module.scss';

const Tickets = ({
  day = null,
  tickets = [ ]
}) => {

  function renderTickets() {

    return tickets.map(({ id, name, startDate, endDate }, i) => {

      const startOfWeek      = day.clone().startOf('week'),
            endOfWeek        = day.clone().endOf('week'),
            isFirstDayOfWeek = day.isSame(startOfWeek, 'day');

      const ticketExtendsBeforeWeek = moment(startDate).isBefore(startOfWeek, 'day'),
            ticketExtendsPastWeek   = moment(endDate).isAfter(endOfWeek, 'day');

      const isTicketStartDay = (
        day.isSame(startDate, 'day') ||
        ticketExtendsBeforeWeek
      );

      const ticketEnd = ticketExtendsPastWeek ? endOfWeek : endDate;
      const ticketLen = Math.abs(ticketExtendsBeforeWeek ?
        startOfWeek.diff(ticketEnd, 'days') :
        moment(startDate).diff(ticketEnd, 'days')
      ) + 1;

      const isPlaceholderTicket = (
        !isTicketStartDay ||
        (!isFirstDayOfWeek && ticketExtendsBeforeWeek)
      );

      if (isPlaceholderTicket) {
        return (
          <div
            key={`ticketPlaceholder-${day.format('MM-DD-YYYY')}-${id}`}
            className={style.ticketPlaceholder} />
        )
      }

      return (
        <div
          key={`ticket-${day.format('MM-DD-YYYY')}-${id}`}
          className={style.ticket}
          style={{
            width: `calc(${ticketLen * 100}% + ${ticketLen - 1}px - 36px)`
          }}>
          {name}
        </div>
      );

    });

  }

  return (
    <div className={style.wrap}>
      {renderTickets()}
    </div>
  );
}

export default Tickets;
