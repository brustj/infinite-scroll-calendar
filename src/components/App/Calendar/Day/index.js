import React from 'react';
import classNames from 'classnames';
import moment from 'moment';

import Tickets from './Tickets';

import style from './index.module.scss';

const Day = ({
  day = null,
  onLoadRef = null,
  isFirstRef = null,
  isLastRef = null,
  isFirstInRange = false,
  isLastInRange = false,
  tickets = [ ]
}) => {

  const isToday           = day.isSame(moment(), 'day'),
        isFirstOfMonth    = day.date() === 1,
        isFirstOfCurMonth = (isFirstOfMonth && day.isSame(moment(), 'month')),
        isPastDay         = (day.isBefore(moment(), 'month') || day.isBefore(moment(), 'year'));

  function getRef() {
    if (isFirstInRange) return isFirstRef;
    if (isLastInRange) return isLastRef;
    if (isFirstOfCurMonth) return onLoadRef;
    return null;
  }

  return (
    <div
      ref={getRef()}
      className={classNames({
        [style.wrap]: true,
        [style.isPastDay]: isPastDay,
        [style.isToday]: isToday
      })}>

      <div className={style.dayHeader}>
        <div className={style.badge}>
          {isFirstOfMonth && `${day.format('MMMM')} `}{day.date()}
        </div>
      </div>

      <Tickets
        day={day}
        tickets={tickets} />

    </div>
  );
}

export default Day;
