
/* eslint-disable react-hooks/exhaustive-deps */

import React, { useEffect, useState, useRef } from 'react';
import moment from 'moment';
import { useInView } from 'react-intersection-observer';
import { useScroll } from 'react-use';
import { sortBy } from 'lodash';

import Day from './Day';

import {
  getDaysBetweenDates,
  getWeeksInRange,
  getWeekRange
} from 'utils/dates';

import ticketData from 'data/tickets.json';

import style from './index.module.scss';

const Calendar = ({
  onUpdateMonthHeader = () => { }
}) => {

  const calRef = useRef(null);
  const onLoadRef = useRef();

  const { y: calScrollY } = useScroll(calRef);

  // TODO: Observer still isn't 100% accurate with speedy scrolling
  const [ isFirstRef, isFirstInView, ] = useInView();
  const [ isLastRef, isLastInView, ] = useInView();

  const defaultRangeStart = moment().subtract(2, 'months').startOf('month').valueOf();
  const defaultRangeEnd = moment().add(2, 'months').endOf('month').valueOf();

  const [ rangeStart, setRangeStart ] = useState(defaultRangeStart);
  const [ rangeEnd, setRangeEnd ] = useState(defaultRangeEnd);

  const startDate = moment(rangeStart),
        endDate   = moment(rangeEnd),
        dateRange = getDaysBetweenDates(startDate, endDate),
        weekRange = getWeeksInRange(dateRange);

  const weeks = weekRange.map((week) => getWeekRange(week));

  useEffect(() => {
    onLoadRef.current.scrollIntoView();
  }, [ ]);

  useEffect(() => {

    if (isFirstInView) {
      setRangeStart( moment(rangeStart).subtract(1, 'months').startOf('month').valueOf() );
      setRangeEnd( moment(rangeEnd).subtract(1, 'months').endOf('month').valueOf() );
    }

    if (isLastInView) {
      setRangeStart( moment(rangeStart).add(1, 'months').startOf('month').valueOf() );
      setRangeEnd( moment(rangeEnd).add(1, 'months').endOf('month').valueOf() );
    }

  }, [
    isFirstInView,
    isLastInView
  ]);

  useEffect(() => {

    const { height: calDisplayH } = calRef?.current?.getBoundingClientRect();
    const calScrollH = calRef?.current?.scrollHeight;

    const rangeIndex = Math.ceil(
      (calScrollY + (calDisplayH / 2)) / calScrollH * dateRange?.length
    );

    const curMonth    = dateRange[rangeIndex],
          monthHeader = curMonth?.format('MMMM YYYY');

    onUpdateMonthHeader(monthHeader);

  }, [ calScrollY ]);

  return (
    <div
      ref={calRef}
      className={style.wrap}>

      {weeks.map((week, weekIndex) => {

        const weekKey = `week-of-${week[0].format('MM-DD-YYYY')}`;

        const weekStart = week[0],
              weekEnd   = week[6];

        const sortedTickets = sortBy(ticketData, ['startDate']);
        const weekTickets = sortedTickets?.filter((t) => (
          weekStart.isBetween(t?.startDate, t?.endDate, undefined, []) ||
          weekEnd.isBetween(t?.startDate, t?.endDate, undefined, []) ||
          moment(t?.startDate).isBetween(weekStart.startOf('day'), weekEnd.endOf('day'), undefined, []) ||
          moment(t?.endDate).isBetween(weekStart.startOf('day'), weekEnd.endOf('day'), undefined, [])
        ));

        return (
          <div
            key={weekKey}
            className={style.wrapWeek}>

            {week.map((day, dayIndex) => {

              const isFirstInRange = (
                weekIndex === 1 &&
                dayIndex === 0
              );

              const isLastInRange = (
                weekIndex === (weeks.length - 2) &&
                dayIndex === (week.length - 1)
              );

              const dayKey = `day-${day.format('MM-DD-YYYY')}`;

              const dayTickets = weekTickets?.filter((t) => (
                day.isBetween(t?.startDate, t?.endDate, undefined, [])
              ));

              return (
                <Day
                  key={dayKey}
                  day={day}
                  onLoadRef={onLoadRef}
                  isFirstRef={isFirstRef}
                  isLastRef={isLastRef}
                  isFirstInRange={isFirstInRange}
                  isLastInRange={isLastInRange}
                  tickets={dayTickets} />
              );

            })}

          </div>
        );

      })}

    </div>
  );
}

export default Calendar;
